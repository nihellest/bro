#
# Variables related functions
#

# Global variables

get_global_vars_keys() {
    # Get key names from vars object.
    # Return plaintext list.
    #
    # Need no arguments.

    jq -r ' .vars | keys_unsorted[] ' ${CONFIG}
}

get_global_var_by_name() {
    # Get value from root vars object by var name
    #
    # Usage:
    #   get_global_var_by_name <var_name>

    var=$1

    jq -r ".vars" ${CONFIG} | jq -r ".$var"
}

get_global_vars() {
    # Function for export additional variables from root vars object.
    # You can use this variables in your custom action function.
    # Variables will be accessible with `VAR_` prefix and uppercased name.
    # Variables can be overrided by repo variables.
    #
    # Example:
    #   {
    #       "vars": {
    #           "foo": "bar",
    #           "baz": 1000
    #       }
    #   }
    #
    #   Will be VAR_FOO="bar" and VAR_BAZ="1000"
    #
    # Usage:
    #   get_global_vars <repo_name>

    for var in $(get_global_vars_keys)
    do
        cap_var=$(echo "\"$var\"" | jq -r "ascii_upcase")
        export VAR_${cap_var}=$(get_global_var_by_name ${var})
    done
}

# Repo variables

get_vars_keys() {
    # Get key names from specific repo vars object.
    # Return plaintext list.
    #
    # Usage:
    #   get_vars_keys <repo_name>

    repo=$1

    get_repo_by_name $repo | jq -r ' .vars | keys_unsorted[]'
}

get_var_by_name() {
    # Get value from vars object by repo name & var name
    #
    # Usage:
    #   get_var_by_name <repo_name> <var_name>

    repo=$1
    var=$2

    get_repo_by_name $repo | jq -r ".vars" | jq -r ".$var"
}

get_vars() {
    # Function for export additional variables from repo object.
    # You can use this variables in your custom action function 
    # for specific repo.
    # Variables will be accessable with `VAR_` prefix and uppercase name.
    #
    # Example:
    #   {
    #       "name": ...,
    #       "refs": ...,
    #       "vars": {
    #           "foo": "bar",
    #           "baz": 1000
    #       }
    #   }
    #
    #   Will be VAR_FOO="bar" and VAR_BAZ="1000"
    #
    # Usage:
    #   get_vars <repo_name>

    repo=$1
    for var in $(get_vars_keys $repo)
    do
        cap_var=$(echo "\"$var\"" | jq -r "ascii_upcase")
        export VAR_${cap_var}=$(get_var_by_name ${repo} ${var})
    done
}
