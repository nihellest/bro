CL_ESC='\e[0m'
CL_RED='\e[31m'
CL_GREEN='\e[32m'
CL_YELLOW='\e[33m'
CL_BLUE='\e[34m'
CL_MAGENTA='\e[35m'
CL_CYAN='\e[36m'
CL_LIGHT_GRAY='\e[37m'

clean() {
    # Function for cleaning all kind of stuff used by script

    # Clean custom variables
    unset $(env | grep VAR_ | sed "s/=.*//g")
    # Clean temp repo directory
    rm -rf ${REPO_DIR}
}

err() {
    echo -e ${CL_RED}$1${CL_ESC}
    clean
    exit 1
}