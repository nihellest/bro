#
# Branches related function
#

get_refs() {
    # Get refs list from repo object, mainly for loops
    # Returns plaintext list
    #
    # Usage:
    #   get_refs <repo_name>

    repo=$1
    get_repo_by_name $repo | jq -r '.refs[]' 
}

ref_checkout() {
    # Switching between repo branches/refs
    # (made this for custom output only)
    #
    # Usage:
    #   ref_checkout <ref_name>

    ref=$1
    echo -e "${CL_CYAN}Switch to${CL_ESC} $ref ${CL_CYAN}branch.${CL_ESC}"
    git checkout --quiet $ref || err "Wrong ref"
}
