#
# Repository related functions
#

prepare_repo() {
    # Function for creating working environment
    # 
    # Usage:
    #   prepare_repo <repo_name>

    repo=$1

    # Basic preparations
    clean
    REPO_URL="${REPO_URL_PREFIX}${repo}"
    echo -e ${CL_GREEN}Processing repository:${CL_ESC} ${REPO_URL}
    mkdir -p ${REPO_DIR}
    git clone --quiet ${REPO_URL} ${REPO_DIR}
    
    # Load environment variables from config
    get_global_vars
    get_vars $repo
    env | grep VAR_
}

get_repos() {
    # Get list of repo names, mainly for loops.
    #
    # Need no args

    jq -r '.repos[] | .name' ${CONFIG}
}

get_repo_by_name() {
    # Function for get repo object from config.json.
    # Returns json.
    #
    # Usage:
    #   get_repo_by_name <repo_name> | jq <your_request>

    repo=$1
    jq --arg repo "$repo" -r ' .repos[] | select(.name == $repo)' ${CONFIG}
}