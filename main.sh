#!/usr/bin/env bash

# Constants

CONFIG_NAME="sac-conf.json"

SCRIPT_PATH=$(pwd)
REPO_DIR="${SCRIPT_PATH}/temp_repo"
CONFIG="${SCRIPT_PATH}/${CONFIG_NAME}"
REPO_URL_PREFIX="$(jq -r ".prefix" ${CONFIG})"

# Imports

. ./lib/util.sh
. ./lib/repo.sh
. ./lib/refs.sh
. ./lib/vars.sh
. action.sh

for repo in $(get_repos)
do
    prepare_repo $repo
    
    cd ${REPO_DIR}
    for ref in $(get_refs $repo)
    do
        ref_checkout $ref
        repo_action
    done
    cd ${SCRIPT_PATH}
    clean
done
